package u06lab.code

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Int, y: Int): Option[Player] = board.find(m => m.x == x && m.y == y).map(m => m.player)

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    for {
      x <- 0 to 2
      y <- 0 to 2
      if find(board, x, y).isEmpty
    } yield Mark(x,y,player) :: board
  }

  def computeAnyGame(player: Player, moves: Int): LazyList[Game] = moves match {
    case n if n > 0 =>
      val (endedGames, notEndedGames) = computeAnyGame(player.other, moves - 1) partition(g => checkWin(g.head))
      endedGames ++ (for(g <- notEndedGames; boards <- placeAnyMark(g.head, player)) yield boards :: g)
    case _ => LazyList(List(Nil))
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 8) foreach {g => printBoards(g); println()} // 10 moves for an entire game session (the board is totally filled)
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
  def checkWin(board: Board): Boolean = board match {
    case hasWinner(_) => true
    case _ => false
  }

  object hasWinner {
    def unapply(board: Board): Option[Player] = {
      getBoardRows(board) ++ getBoardColumns(board) ++ getBoardDiagonals(board) collectFirst(
        {case players: List[Player] if players.size == 3 && players.forall(players.head == _) => players.head}
      )
    }
  }

  def getBoardRows(board: Board): List[List[Player]] = for(x <- (0 to 2).toList) yield board collect {case Mark(`x`, _, player) => player}
  def getBoardColumns(board: Board): List[List[Player]] = for(y <- (0 to 2).toList) yield board collect {case Mark(_, `y`, player) => player}
  def getBoardDiagonals(board: Board): List[List[Player]] = List(
    board collect {case Mark(x, y, player) if x == y => player},
    board collect {case Mark(x, y, player) if x+y == 2 => player}
  )

}
